import random

import redis
from dj_rest_auth.serializers import PasswordResetSerializer
from django.contrib.auth.hashers import make_password
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from users.models import User
from .serializers import PasswordResetConfirmSerializer


class SendUrlAPIView(APIView):
    serializer_class = PasswordResetSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        email = serializer.validated_data['email']
        user = User.objects.filter(email=email).first()
        redis_instance = redis.StrictRedis(host='localhost', port=6379, db=0)

        if user:
            code = random.randint(100000, 999999)
            rcode = redis_instance.set(f'code:{code}', code, ex=600)
            print(rcode)

            html_content = render_to_string('emails/confirmation_email.html', {'code': code})
            text_content = strip_tags(html_content)

            send_mail(
                'Код подтверждения электронной почты',
                text_content,
                'abdusalomabduvaliyev07@gmail.com',
                [email],
                html_message=html_content,
            )

            return Response({'detail': 'Password reset code sent. Check your inbox.'}, status=status.HTTP_200_OK)

        return Response({'detail': 'User not found.'}, status=status.HTTP_400_BAD_REQUEST)


class ResetPasswordConfirmAPIView(CreateAPIView):
    serializer_class = PasswordResetConfirmSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        email = serializer.validated_data['email']
        code = serializer.validated_data['code']

        rcode = redis.StrictRedis(host='localhost', port=6379, db=0).get(f"code:{code}").decode('utf-8')

        if rcode == code:
            user = User.objects.filter(email=email).first()
            new_password = self.request.data.get('new_password')

            password = make_password(new_password)
            user.password = password
            user.save()
        else:
            raise Exception("Invalid code")

        return Response({"email_address": email}, status=status.HTTP_200_OK)
