from rest_framework.serializers import ModelSerializer

from users.models import User


class UserUpdateSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = [
            'full_name',
            'email',
        ]
