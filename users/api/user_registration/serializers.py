from rest_framework import serializers

from users.models import User


class UserRegistrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['full_name', 'email', 'password']


class EmailConfirmationSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    code = serializers.IntegerField(required=True)

    def validate(self, attrs):
        email = attrs.get('email')
        code = attrs.get('code')

        return attrs
