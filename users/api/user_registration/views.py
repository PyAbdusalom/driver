import random

import redis
from django.contrib.auth.hashers import make_password
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken

from users.models import User
from .serializers import UserRegistrationSerializer, EmailConfirmationSerializer


class UserRegistrationAPIView(CreateAPIView):
    serializer_class = UserRegistrationSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        validated_data = serializer.validated_data

        if validated_data['phone_number'] is None:
            return Response({'error': 'Please provide a valid phone number'}, status=status.HTTP_400_BAD_REQUEST)

        redis_instance = redis.StrictRedis(host='localhost', port=6379, db=0)

        user_id = redis_instance.incr(f'user_id:{validated_data["email"]}')

        password = make_password(validated_data['password'])

        user_data = {
            'full_name': validated_data['full_name'],
            'email': validated_data['email'],
            'password': password,
        }

        redis_instance.hmset(f"user:{user_id}", user_data)

        code = random.randint(100000, 999999)

        html_content = render_to_string('emails/confirmation_email.html', {'code': code})
        text_content = strip_tags(html_content)

        send_mail(
            'Код подтверждения электронной почты',
            text_content,
            'abdusalomabduvaliyev07@gmail.com',
            [validated_data['email']],
            html_message=html_content,
        )

        return Response({'user_id': user_id, 'email': validated_data['email']}, status=status.HTTP_201_CREATED)


class EmailConfirmationView(CreateAPIView):
    serializer_class = EmailConfirmationSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        email = serializer.validated_data['email']
        code = serializer.validated_data['code']

        try:
            user_id = redis.StrictRedis(host='localhost', port=6379, db=0).get(f"user_id:{email}")

            if user_id:
                user_data = redis.StrictRedis(host='localhost', port=6379, db=0).hgetall(
                    f"user:{user_id.decode('utf-8')}")
                user_data = {key.decode('utf-8'): value.decode('utf-8') for key, value in user_data.items()}

                user = User.objects.create(
                    first_name=user_data['full_name'],
                    email=user_data['email'],
                    password=user_data['password']
                )
                user.save()

                refresh = RefreshToken.for_user(user)
                tokens = {
                    'refresh': str(refresh),
                    'access': str(refresh.access_token),
                }

                return Response({'tokens': tokens}, status=status.HTTP_200_OK)

            else:
                return Response({'message': 'User with this email does not exist in Redis.'},
                                status=status.HTTP_404_NOT_FOUND)

        except User.DoesNotExist:
            return Response({'message': 'User with this email does not exist in the database.'},
                            status=status.HTTP_404_NOT_FOUND)
