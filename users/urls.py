from django.urls import path

from users.api.reset_password.views import SendUrlAPIView, ResetPasswordConfirmAPIView
from users.api.user_registration.views import UserRegistrationAPIView, EmailConfirmationView
from users.api.user_retrieve.views import UserRetrieveView
from users.api.user_update.views import UserUpdateAPIView

urlpatterns = [
    path('register/', UserRegistrationAPIView.as_view(), name='user-register'),
    path('confirm-email/', EmailConfirmationView.as_view(), name='email-confirm'),
    path('send-url/', SendUrlAPIView.as_view(), name='send-url'),
    path('get-me/', UserRetrieveView.as_view(), name='get-me'),
    path('user-update/', UserUpdateAPIView.as_view(), name='user-update'),
    path('reset-password/', ResetPasswordConfirmAPIView.as_view(), name='reset-password')
]
