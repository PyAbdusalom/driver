from django.db import models

from users.models import User


def user_directory_path(instance, filename):
    return f"files/{instance.filename}/{filename}"


class File(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE)
    filename = models.CharField(max_length=125)
    file = models.FileField(upload_to=user_directory_path)
    uploaded_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        app_label = 'files'
        db_table = 'files'
        verbose_name = 'File'
        verbose_name_plural = 'Files'

    def __str__(self):
        return self.filename
