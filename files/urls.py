from django.urls import path

from files.views import FileCreateView, FileAllListView, FileListView, FileUpdateAPIView, FileDeleteAPIView

urlpatterns = [
    path('file_upload/', FileCreateView.as_view(), name='file-upload'),
    path('files_for_read_only/', FileAllListView.as_view(), name='files-for-read-only'),
    path('files_list/', FileListView.as_view(), name='files-list'),
    path('file_update/<int:pk>/', FileUpdateAPIView.as_view(), name='file-update'),
    path('file_delete/<int:pk>/', FileDeleteAPIView.as_view(), name='file-delete'),
]
