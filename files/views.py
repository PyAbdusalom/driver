from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework.generics import DestroyAPIView
from rest_framework.generics import UpdateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response

from .models import File
from .serializers import FileSerializer


class FileCreateView(CreateAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializer


class FileAllListView(ListAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class FileListView(ListAPIView):
    serializer_class = FileSerializer

    def get_queryset(self):
        return File.objects.filter(user=self.request.user)


class FileUpdateAPIView(UpdateAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializer
    permission_classes = [IsAuthenticated]
    lookup_field = 'pk'

    def put(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)

    def patch(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.put(request, *args, **kwargs)


class FileDeleteAPIView(DestroyAPIView):
    queryset = File.objects.all()
    serializer_class = FileSerializer
    permission_classes = [IsAuthenticated]
    lookup_field = 'pk'

    def delete(self, request, *args, **kwargs):
        file_instance = self.get_object()

        if file_instance.user != request.user:
            return self.permission_denied(request)

        file_instance.delete()
        return self.destroy(request, *args, **kwargs)

    Response('Successfully deleted')
